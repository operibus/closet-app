/**
 * Created by Albert on 06/05/2016.
 */
app.factory('Usuario', function($resource){
  return $resource('http://localhost:8080/usuario/login/:email/:contrasena',{email:'@email',contrasena:'@contrasena'},{
    get:    {method: 'GET', isArray: false},})
});

app.factory('NewUser', function($resource){
  return $resource('http://localhost:8080/usuario')
});

app.factory('UserProfile', function($resource){
  return $resource('http://localhost:8080/perfil/:id',{id: '@id'},{
    get:    {method: 'GET', isArray: false},})
});

app.factory('Prenda', function($resource){
  return $resource('http://localhost:8080/perfil/prendas/:id',{id: '@id'},{
    get:    {method: 'GET', isArray: true},})
});

app.factory('UpdateUser', function($resource){
  return $resource('http://localhost:8080/usuario/update', {
  });
});

app.factory('UserDelete', function($resource){
  return $resource('http://localhost:8080/usuario/delete/:id')
});

app.factory('AdminUsers', function($resource){
  return $resource('http://localhost:8080/usuario/getAll',{
    get:    {method: 'GET', isArray: true},})
});
