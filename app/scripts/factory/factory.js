app.factory('provincia', function($http) {
  return $http.get('http://localhost:8080/provincia');
});
app.factory('ciudad', function($resource){
    return $resource('http://localhost:8080/provincia/:id', {id:'@id'},{
    get:    {method: 'GET', isArray: true}})
});
app.factory('auto', function($http){
  return $http.get('http://localhost:8080/autocomplete');
});
app.factory('Marcas', function($http){
  return $http.get('http://localhost:8080/newGarment/marcas');
});
app.factory('Tallas', function($http){
  return $http.get('http://localhost:8080/newGarment/tallas');
});
app.factory('TipoPrenda', function($http){
  return $http.get('http://localhost:8080/newGarment/prendas');
});
app.factory('autoCom', function($resource){
  return $resource('http://localhost:8080/autocomplete/search/:nombre/:tipo/:id',{nombre:'@nombre',tipo:'@tipo', id: '@id'},{
    get:    {method: 'GET', isArray: true},})
});

app.factory('image', function($resource){
  return $resource('http://localhost:8080/newGarment/image/:file',{file:'@file'},{
    get:    {method: 'GET', isArray: true},})
});

app.factory('Garment', function($resource){
  return $resource('http://localhost:8080/newGarment/garment');
});

app.factory('CommentPost', function($resource){
  return $resource('http://localhost:8080/newGarment/comments/:usuarioId/:prendaId',{usuarioId:'@usuarioId',prendaId:'@prendaId'},{
    get:    {method: 'GET', isArray: true},})
});
app.factory('CommentGet', function($resource){
  return $resource('http://localhost:8080/newGarment/comments/:prendaId', {prendaId: '@prendaId'},{
    get:    {method: 'GET', isArray: true},})
})
app.factory('GarmentDelete', function($resource){
  return $resource('http://localhost:8080/newGarment/delete/:id')
});
