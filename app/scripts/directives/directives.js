app.directive('buger', function(){
  return{
    restrict: 'E',
    templateUrl: "views/autocomplete.html",
    replace:true
  }
});

angular.module("myapp").directive('dlEnterKey',['$location', function(location, $location) {
  return function(scope, element, attrs) {
    element.bind("keydown keypress", function(event) {
      var keyCode = event.which || event.keyCode;
      if (keyCode === 13) {
        scope.$apply(function() {
          scope.$eval(attrs.dlEnterKey);
          location.path('/searchresults');
        });
        event.preventDefault();
      }
    });
  };
}]);

angular.module("myapp").directive('chooseFileButton', function() {
  return {
    restrict: 'A',
    link: function (scope, elem, attrs) {
      elem.bind('click', function() {
        angular.element(document.querySelector('#' + attrs.chooseFileButton))[0].click();
      });
    }
  };
});
