'use strict';
var app = angular.module('myapp',['ngRoute', 'LocalStorageModule', 'ngMaterial','ngResource', 'material.svgAssetsCache', 'flow']);

app.config(function($routeProvider){
  $routeProvider
    .when('/login', {
      templateUrl : 'views/login.html',  activate: 'none', activetab: 'dashboard'
    })
  .when('/register', {
      templateUrl: 'views/register.html', activate: 'none', activetab: 'dashboard'
    })
    .when('/profile', {
      templateUrl: 'views/profile.html', activate: 'none', activetab: 'dashboard'
    })
    .when('/searchresults', {
      templateUrl: 'views/searchResults.html', activate: 'none', activetab: 'dashboard'
    })
    .when('/new', {
    templateUrl: 'views/createNew.html', activate: 'none', activetab: 'dashboard'
  })
    .when('/tuPerfil', {
      templateUrl: '../../views/yourProfile.html', activate: 'none', activetab: 'dashboard'
    })
    .when('/prenda', {
      templateUrl: '../../views/garment.html', activate: 'none', activetab: 'dashboard'
    })
    .when('/garmentSearch', {
      templateUrl: '../../views/garmentSearch.html', activate: 'none', activetab: 'dashboard'
    })
      .when('/admin', {
        templateUrl: '../../views/admin.html', activate: 'none', activetab: 'dashboard', actadmin: 'admin'
      })
      .when('/adminCreate', {
        templateUrl: '../../views/adminCreate.html', activate: 'none', activetab: 'dashboard', actadmin: 'admin'
      })
      .when('/adminGarment', {
        templateUrl: '../../views/adminGarment.html', activate: 'none', activetab: 'dashboard', actadmin: 'admin'
      })
});

app.controller('main', function ($scope, $location, localStorageService, $route) {
  $scope.currentPath = $location.path();
  $scope.pruebas = localStorageService.get('usuarioCookie');
  $scope.$route = $route;
  $scope.disconnect = function() {
    localStorageService.remove('usuarioCookie');
    location.reload();
  };
  $scope.begin = function () {
    $scope.usuarioLog = localStorageService.get('usuarioCookie');
    if($scope.usuarioLog != undefined){
      $location.path('/new');
    }else{
      $location.path('/login');
    }
  }
    });
app.controller('login', function ($scope, Usuario, $filter, $location, localStorageService) {
  $scope.look = function () {
    if ($scope.loginForm.$valid) {
        var usuario = getFormPost($scope, $filter);
      Usuario.get({email : usuario.email},{contrasena : usuario.contrasena} ).$promise.then(function(data) {
        $scope.usuarioLog = data;
        if($scope.usuarioLog.id != null){
          $scope.usuarioLog.isLogged = true;
          localStorageService.set('usuarioCookie',$scope.usuarioLog);
          if($scope.usuarioLog.tipoUsuario == 2){
            $location.path('/tuPerfil');
            location.reload();
          }else{
            $location.path('/admin');
            location.reload();
          }
        }
  })
}}});
app.controller('DemoCtrl',function(provincia, $scope, ciudad, $filter, NewUser, $location, localStorageService, Usuario) {
$scope.usuarioRegister={
    nombre:"", email:"", provincia:"", contrasena:"", repite:"", ciudad:""};
  provincia.success(function(data) {
    $scope.provincias = data;
    $scope.provincias.map(function(provincia){
      $scope.updateProvincia = function() {
        var identificador = $scope.usuarioRegister.provincia.id;
         ciudad.get({id : identificador}).$promise.then(function(data) {
           $scope.ciudades = data;
           $scope.updateCiudad = function(ciudades){
           }
        });
      }});
  });
  $scope.create = function () {
    if ($scope.loginForm.$valid) {
    var usuario = getRegisterForm($scope, $filter);
    usuario.tipoUsuarioId=2;
    NewUser.save(usuario);
    window.setTimeout(function() {
      Usuario.get({email : usuario.email},{contrasena : usuario.contrasena} ).$promise.then(function(data) {
        $scope.usuarioLog = data;
        if($scope.usuarioLog.id != null){
          $scope.usuarioLog.isLogged = true;
          localStorageService.set('usuarioCookie',$scope.usuarioLog);
          $location.path('/tuPerfil');
          location.reload();
        }
      })
      $location.path('/tuPerfil');
    }, 1000);
  }}
});
app.controller('Appctrl', function($scope, localStorageService){
  $scope.usuarioLog = localStorageService.get('usuarioCookie');
});

app.controller('profile', function($scope, localStorageService, UserProfile, $location){
  $scope.usuario = localStorageService.get('usuarioSearch');
  var identificador = $scope.usuario.id;
  UserProfile.get({id : identificador}).$promise.then(function(data) {
    $scope.usuarioProfile = data;
  });


});
app.controller('escaparate', function($scope, localStorageService, UserProfile, Prenda, $location){
    $scope.usuario = localStorageService.get('usuarioSearch');
    var identificador = $scope.usuario.id;
    Prenda.get({id : identificador}).$promise.then(function(data) {
      $scope.prendaProfile = data;
      angular.forEach($scope.prendaProfile, function(eachObj) {
        eachObj.photo = "data:image/jpg;base64,"+eachObj.foto;
        $scope.prendaProfile.photo =  eachObj.photo;
      });
      $scope.sendGarment = function(prenda){
          localStorageService.set('garment',prenda);
          $location.path('/prenda');
        }
      })
  });

app.controller('myProfile', function($scope, localStorageService){
  $scope.usuarioLogger = localStorageService.get('usuarioCookie');
  //$scope.changeFoto = function(){
  //  angular.forEach(files, function(flowFile, i) {
  //    var fileReader = new FileReader();
  //    fileReader.onload = function(event) {
  //      var uri = event.target.result;
  //      $scope.imageStrings[i] = uri;
  //      $scope.imgContent = {
  //        fileName: flowFile.name,
  //        fileContent: uri
  //      };
  //    };
  //    var imgElem = document.getElementById('niceImage').src;
  //    fileReader.readAsDataURL(flowFile.file);
  //    $scope.fileArray.push($scope.imgContent);
  //  });

  //}
});

app.controller('changeData', function($scope, localStorageService, provincia, ciudad, UpdateUser, UserProfile) {
  $scope.usuarioLogger = localStorageService.get('usuarioCookie');
  provincia.success(function (data) {
    $scope.provincias = data;
    $scope.provincias.map(function (provincia) {
      $scope.updateProvincia = function () {
        var identificador = $scope.usuariochange.provincia.id;
        ciudad.get({id: identificador}).$promise.then(function (data) {
          $scope.ciudades = data;
          $scope.updateCiudad = function (ciudades) {
          }
        });
      }
    });
  });
  $scope.changeData = function(){
    var id =  $scope.usuarioLogger.id;
    if( "ciudad" in $scope.usuariochange){

      var ciudad =$scope.usuariochange.ciudad.id;

    }else{
      var ciudad = $scope.usuarioLogger.ciudad.id
    }

    if("nombre" in $scope.usuariochange){
      var nombre =$scope.usuariochange.nombre;
    }else{
      var nombre = $scope.usuarioLogger.nombre;
    }

    if("email" in $scope.usuariochange){

      var email = $scope.usuariochange.email;

    }else{

      var email = $scope.usuarioLogger.email;

    }

    var usuario = {
      nombre : nombre,
      email: email,
      ciudadId : ciudad,
      id: id
    }
    UpdateUser.save(usuario);
   var identificador = $scope.usuarioLogger.id;
    window.setTimeout(function() {
      UserProfile.get({id: identificador}).$promise.then(function (data) {
        $scope.user = data;
        $scope.user.isLogged= true;
        localStorageService.set('usuarioCookie', $scope.user);
      });
    }, 500);
    window.setTimeout(function() {
      location.reload();
    }, 1000);
  };
  $scope.Confirm = function(ev) {
    var confirm = $mdDialog.confirm()
      .title('Eliminar prenda')
      .textContent('¿Seguro que quieres eliminar todos tus datos?')
      .ariaLabel('Lucky day')
      .targetEvent(ev)
      .ok('No')
      .cancel('Sí');

    $mdDialog.show(confirm).then(function() {

    }, function() {
      var usuario = {
        id: $scope.usuarioLogger.id
      };
      localStorageService.remove('usuarioCookie');
      UserDelete.remove(usuario);
      $location.path('#/')
      window.setTimeout(function() {
        location.reload();
      }, 500);
    });
  };
  $scope.hide = function() {
    $mdDialog.hide();
  };

  $scope.cancel = function() {
    $mdDialog.cancel();
  };

  $scope.answer = function(answer) {
    $mdDialog.hide(answer);
  };
});


app.controller('myGarment', function($scope, localStorageService, Prenda, $location, $mdDialog, GarmentDelete){
  $scope.usuarioLogger = localStorageService.get('usuarioCookie');
  var identificador = $scope.usuarioLogger.id;
  Prenda.get({id : identificador}).$promise.then(function(data) {
    $scope.prendaProfile = data;
    angular.forEach($scope.prendaProfile, function(eachObj) {
      eachObj.photo = "data:image/jpg;base64,"+eachObj.foto;
      $scope.prendaProfile.photo =  eachObj.photo;
    });
    $scope.sendGarment = function(prenda){
      localStorageService.set('garment',prenda);
      $location.path('/prenda');
    }
  });
  $scope.showConfirm = function(ev, prenda) {
    var confirm = $mdDialog.confirm()
      .title('Eliminar prenda')
      .textContent('¿Seguro que quieres eliminar todos tus datos?')
      .ariaLabel('Lucky day')
      .targetEvent(ev)
      .ok('No')
      .cancel('Sí');

    $mdDialog.show(confirm).then(function() {

    }, function() {
      var garment = {
        id: prenda.id
      };
      GarmentDelete.remove(garment);
      window.setTimeout(function() {
        location.reload();
      }, 500);
    });
  };
  $scope.hide = function() {
    $mdDialog.hide();
  };

  $scope.cancel = function() {
    $mdDialog.cancel();
  };

  $scope.answer = function(answer) {
    $mdDialog.hide(answer);
  };
});

app.controller('eliminar', function($scope, $mdDialog, UserDelete, $location, localStorageService) {
    $scope.showConfirm = function(ev) {
      var confirm = $mdDialog.confirm()
        .title('Eliminar cuenta')
        .textContent('¿Seguro que quieres eliminar todos tus datos?')
        .ariaLabel('Lucky day')
        .targetEvent(ev)
        .ok('No')
        .cancel('Sí');

      $mdDialog.show(confirm).then(function() {

      }, function() {
        var usuario = {
          id: $scope.usuarioLogger.id
        };
        localStorageService.remove('usuarioCookie');
        UserDelete.remove(usuario);
        $location.path('#/')
        window.setTimeout(function() {
          location.reload();
        }, 500);
      });
    };
$scope.hide = function() {
  $mdDialog.hide();
};

$scope.cancel = function() {
  $mdDialog.cancel();
};

$scope.answer = function(answer) {
  $mdDialog.hide(answer);
};
});


function getRegisterForm($scope){
  var ciudad =$scope.usuarioRegister.ciudad.id;
  var usuario = {
    nombre : $scope.usuarioRegister.nombre,
    email: $scope.usuarioRegister.email,
    contrasena : $scope.usuarioRegister.contrasena,
    ciudadId : ciudad
  }
  return usuario;
}

function getFormPost($scope){
  var usuario = {
    email: $scope.usuarioLogin.email,
    contrasena: $scope.usuarioLogin.contrasena
  };
  return usuario;
};

