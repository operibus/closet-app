(function () {
  app.controller('MosheController', function ($scope, auto) {
    auto.success(function(data) {
    $scope.myDta = data;
      $scope.autoComp=[];
      var i =0;
      var provincias = $scope.myDta.provincias;
      angular.forEach(provincias, function(eachObj) {
        eachObj.tipo ='provincia';
        $scope.autoComp.push(provincias[i]);
        i+=1;
      });
      i=0;
      var usuarios = $scope.myDta.usuarios;
      angular.forEach(usuarios, function(eachObj) {
        eachObj.tipo ='usuario';
        $scope.autoComp.push(usuarios[i]);
        i+=1;
      });
      i=0;
      var tallas = $scope.myDta.tallas;
      angular.forEach(tallas, function(eachObj) {
        eachObj.tipo ='talla';
        $scope.autoComp.push(tallas[i]);
        i+=1;
      });
      i=0;
      var tipoPrenda = $scope.myDta.tipoPrenda;
      angular.forEach(tipoPrenda, function(eachObj) {
        eachObj.tipo ='tipoPrenda';
        $scope.autoComp.push(tipoPrenda[i]);
        i+=1;
      });
    $scope.getMatches = function (text) {
      text = text.toLowerCase();
      var ret = $scope.autoComp.filter(function (d) {
        return d.nombre.toLowerCase().startsWith(text);
      });
      return ret;
    }
  });
})
})();
