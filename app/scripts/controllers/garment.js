/**
 * Created by Albert on 29/05/2016.
 */
app.controller('garment',function($scope, localStorageService, CommentPost, CommentGet) {
  $scope.garmentProfile = localStorageService.get('garment');
  $scope.usuarioLogged = localStorageService.get('usuarioCookie');
  CommentGet.get({prendaId: $scope.garmentProfile.id}).$promise.then(function(data) {
      $scope.comentarios = data;
  });
  $scope.putComment = function(index){
    var comentario ={
      usuarioId: $scope.usuarioLogged.id,
      texto: $scope.newComment.texto,
      prendaId: $scope.garmentProfile.id
    }
    var comentarioNoReload={
      usuarioId: $scope.usuarioLogged.nombre,
      texto: $scope.newComment.texto,
      prendaId: $scope.garmentProfile.id
    }
    $scope.comentarios.push(comentarioNoReload);
    CommentPost.save(comentario);
  }
});
