/**
 * Created by Albert on 05/06/2016.
 */
app.controller('administrador', function($scope, AdminUsers, UserDelete, localStorageService, $location) {
    $scope.usuarioLogger = localStorageService.get('usuarioCookie');
    if($scope.usuarioLogger == null || $scope.usuarioLogger.tipoUsuario != 1){
        $location.path('#/login');
    }
    AdminUsers.query().$promise.then(function(data) {
        $scope.usuarios = data;
    });
    $scope.deleteUser = function(usuario){
        var user = {
            id: usuario.id
        };
        UserDelete.remove(user);
        window.setTimeout(function() {
            location.reload();
        }, 500);
    }
    $scope.adminCreate = function(){
        $location.path("/adminCreate");
    }
    $scope.garmentUser = function(usuario){
        localStorageService.set('usuarioAdmin', usuario);
        $location.path("/adminGarment");
    }
});

app.controller('adminCreate', function($scope, AdminUsers, UserDelete, localStorageService, $location, provincia, ciudad,
                                       $filter, Usuario, NewUser) {
    $scope.usuarioLogger = localStorageService.get('usuarioCookie');
    if($scope.usuarioLogger == null || $scope.usuarioLogger.tipoUsuario != 1){
        $location.path('#/login');
    }
    $scope.usuarioRegister={
        nombre:"", email:"", provincia:"", contrasena:"", repite:"", ciudad:""};
    provincia.success(function(data) {
        $scope.provincias = data;
        $scope.provincias.map(function(provincia){
            $scope.updateProvincia = function() {
                var identificador = $scope.usuarioRegister.provincia.id;
                ciudad.get({id : identificador}).$promise.then(function(data) {
                    $scope.ciudades = data;
                    $scope.updateCiudad = function(ciudades){
                    }
                });
            }});
    });
    $scope.create = function () {
        var usuario = getRegisterForm($scope, $filter);
        if($scope.data.group1 == "Administrador"){
            usuario.tipoUsuarioId = "1";
        }else{
            usuario.tipoUsuarioId = "2";
        }
        NewUser.save(usuario);
        $location.path('/admin');
      window.setTimeout(function() {
        location.reload();
      }, 500);
    }
});

app.controller('administradorPrendas', function($scope, localStorageService, Prenda, $location, GarmentDelete ){
    $scope.usuarioLogger = localStorageService.get('usuarioCookie');
    if($scope.usuarioLogger == null || $scope.usuarioLogger.tipoUsuario != 1){
        $location.path('#/login');
    }
    var usuario = localStorageService.get('usuarioAdmin');
    var identificador = usuario.id;
        Prenda.get({id : identificador}).$promise.then(function(data) {
        $scope.prendaProfile = data;
        angular.forEach($scope.prendaProfile, function (eachObj) {
            eachObj.photo = "data:image/jpg;base64," + eachObj.foto;
            $scope.prendaProfile.photo = eachObj.photo;
        });
    });
    $scope.turnBack = function(){
        $location.path("/admin");
    }
    $scope.deletePrenda = function(prenda){
        var garment = {
            id: prenda.id
        };
        GarmentDelete.remove(garment);
        window.setTimeout(function() {
            location.reload();
        }, 500);
    }
});
