
  app.controller('AppCtrl', function($scope, autoCom, $location, localStorageService) {
    var auto ={
      nombre: $scope.selectedItem.nombre,
      id: $scope.selectedItem.id,
      tipo: $scope.selectedItem.tipo
    };

    autoCom.get({nombre: auto.nombre, tipo: auto.tipo, id: auto.id}).$promise.then(function(data) {
      $scope.todos = data;
      angular.forEach($scope.todos, function(eachObj) {
        eachObj.foto = "data:image/jpg;base64,"+eachObj.foto;
      });
      var tipo;
      var i =0;
      if($scope.todos[0].tallaId){
        tipo = "prenda";
      }else{
        tipo = "usuario";
      }
      angular.forEach($scope.todos, function(eachObj) {
        eachObj.tipo = tipo;
        i+=1;
      });
    })

    $scope.clickSearch = function(item, tipo){
        if (item.tipo == "usuario"){
          localStorageService.set('usuarioSearch',item);
          $location.path('/profile');
        }else{
         item.photo = item.foto;
          localStorageService.set('garment',item);
          $location.path('/prenda');
        }
    }
  });
