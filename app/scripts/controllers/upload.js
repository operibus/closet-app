app.controller('uploadUser', function($scope, Marcas, Tallas, TipoPrenda, image, localStorageService, Garment, $location) {

  Marcas.success(function(data) {
    $scope.marcas = data;
  })
  Tallas.success(function(data) {
    $scope.tallas = data;
  })
  TipoPrenda.success(function(data) {
    $scope.ofrecidas = data;
    $scope.tipos = data;
  })

  $scope.fileArray = [];
  $scope.imgContent = {};
  $scope.imageStrings = [];


  $scope.processFiles = function(files) {

    angular.forEach(files, function(flowFile, i) {
      var fileReader = new FileReader();
      fileReader.onload = function(event) {
        var uri = event.target.result;
        $scope.imageStrings[i] = uri;
        $scope.imgContent = {
          fileName: flowFile.name,
          fileContent: uri
        };
      };

      fileReader.readAsDataURL(flowFile.file);
      $scope.fileArray.push($scope.imgContent);
    });
  };
  $scope.begin = function(){
    var prenda = getGarmentPost($scope, localStorageService);
    Garment.save(prenda);
    window.setTimeout(function() {
        var file = document.getElementById('imageThum').getAttribute('src');
        var imageBase64 = file.replace(/^data:image\/(png|jpeg);base64,/, "");
        image.save(imageBase64);
      $location.path("/tuPerfil");
    }, 1000);
    window.setTimeout(function() {
      location.reload();
    }, 3000)
  }
});


function getGarmentPost($scope, localStorageService) {
  var marcaSelect = $scope.uploadGarment.marca.id;
  var tallaSelect = $scope.uploadGarment.talla.id;
  var tipoPrendaOfrecidaSelect = $scope.uploadGarment.ofrecida.id;
  var tipoPrendaIdSelect = $scope.uploadGarment.tipo.id;
  $scope.usuarioLog = localStorageService.get('usuarioCookie');
  var garment = {
    descripcion: $scope.uploadGarment.descripcion,
    tallaId: tallaSelect,
    marcaId: marcaSelect,
    tipoPrendaOfrecida: tipoPrendaOfrecidaSelect,
    tipoPrendaId: tipoPrendaIdSelect,
    usuarioId:  $scope.usuarioLog.id
  }
  return garment;
}
